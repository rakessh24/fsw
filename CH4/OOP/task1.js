const Car = function (make, speed) {
    this.make = make;
    this.speed = speed;
  };
  
  Car.prototype.accelerate = function () {
    this.speed += 0;
    console.log(`${this.make} is going at ${this.speed} km/h`);
  };
  
  Car.prototype.brake = function () {
    this.speed -= 5;
    console.log(`${this.make} is going at ${this.speed} km/h`);
  };
  
  /**
   * Inisiasi with keyword new
   * Your code here (untuk variable bmw & mercedes)
   */

  const bmw = new Car("bmw", 130);
  const mercedes = new Car("mercedes", 105);
  
  bmw.accelerate();
  mercedes.accelerate();
  bmw.brake();
  mercedes.brake();
  
  /**
   * Output: bmw is going 130
   * Output: mercedes is going 105
   * Output: bmw is going 125
   * Output: mercedes is going 100
   */
  