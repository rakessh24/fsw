// #1 Gunakan metode untuk membalikan isi array

let fruits = ["Apple", "Banana", "Papaya", "Grape", "Cherry", "Peach"];

fruits. reverse();
fruits.forEach(function(item) {
    console.log(item)
})

/* 
Output: 
Peach
Cherry
Grape
Papaya
Banana
Apple
*/