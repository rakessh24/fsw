// #1 Gunakan metode untuk membalikan isi array

let fruits = ["Apple", "Banana", "Papaya", "Grape", "Cherry", "Peach"];

fruits. reverse();
fruits.forEach(function(item) {
    console.log(item)
})

/* 
Output: 
Peach
Cherry
Grape
Papaya
Banana
Apple
*/

// #2 Gunakan metode untuk menyisipkan elemen baru dalam array

let month = [
  "January",
  "February",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

month.splice(2, 0, "March", "April", "May", "June");
console.log(month);

/* 
Output: 
 ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
*/

// #3 Mengambil beberapa kata terakhir dalam string
const message = "Sampaikan pada Sabrina belajar Javascript sangat menyenangkan";

let text = message.slice(23, 61);

console.log(text)
/* 
Output: belajar Javascript sangat menyenangkan
*/

// #4 Buat agar awal kalimat jadi kapital

let text1 = message.slice(23,24).toUpperCase();
let text2 = message.slice(24);

console.log(text1 + text2);
/* 
Output: Belajar Javascript sangat menyenangkan
*/

/* #5 Buat function untuk menghitung BMI (Body Mass Index)
- BMI = mass / height ** 2 = mass / (height * height) (mass in kg and height in meter)
- Steven weights 78 kg and is 1.69 m tall. Bill weights 92 kg and is 1.95 
m tall
*/

function BMI(mass, height){
  let bmi = mass / (height ** 2);
  return bmi;
}

// Menghitung BMI Steven
let stevenmass = 78;
let stevenheight = 1.69;
let stevenbmi = BMI(stevenmass, stevenheight);
console.log("Steven BMI: ", stevenbmi);

// Menghitung BMI Bill
let billmass = 92;
let billheight = 1.95;
let billbmi = BMI(billmass, billheight);
console.log("Bill BMI: ", billbmi);

/* #6 Menghitung BMI dengan if else statement
- John weights 95 kg and is 1.88 m tall. Nash weights 85 kg and is 1.76 
m tall

Output example:
John's BMI (28.3) is higher than Nash's (23.5)
*/

function BMI(mass, height){
  let bmi = mass / (height ** 2);
  return bmi;
}

// Menghitung BMI John
let johnmass = 95;
let johnheight = 1.88;
let johnbmi = BMI(johnmass, johnheight);

// Menghitung BMI Nash
let nashmass = 85;
let nashheight = 1.76;
let nashbmi = BMI(nashmass, nashheight);

  if (johnbmi > nashbmi) {
    console.log ("John's BMI (" + johnbmi.toFixed(1) + ") is higher than Nash's (" + nashbmi.toFixed(1) +")");
  } else {
    console.log ("Nash's BMI (" + nashbmi.toFixed(1) + ") is higher than John's (" + johnbmi.toFixed(1) + ")");
  }


//  #7 Looping

let data = [10, 20, 30, 40, 50];
let total = 0;

/* You code here (you are allowed to reassigned the variable) 
Maybe you can write 3 lines or more
Use for, do while, while for, or forEach
*/

for (let i = 0; i < data.length; i++){
  total += data[i];
}


console.log(`Jumlah total = ${total}`);
/* 
Jumlah total = 150
*/
