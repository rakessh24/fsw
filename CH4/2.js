// #2 Gunakan metode untuk menyisipkan elemen baru dalam array

let month = [
    "January",
    "February",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  month.splice(2, 0, "March", "April", "May", "June");
  console.log(month);
  
  /* 
  Output: 
   ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
  */