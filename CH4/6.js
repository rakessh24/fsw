/* #6 Menghitung BMI dengan if else statement
- John weights 95 kg and is 1.88 m tall. Nash weights 85 kg and is 1.76 
m tall

Output example:
John's BMI (28.3) is higher than Nash's (23.5)
*/
function BMI(mass, height){
    let bmi = mass / (height ** 2);
    return bmi;
}

// Menghitung BMI John
let johnmass = 95;
let johnheight = 1.88;
let johnbmi = BMI(johnmass, johnheight);

// Menghitung BMI Nash
let nashmass = 85;
let nashheight = 1.76;
let nashbmi = BMI(nashmass, nashheight);

    if (johnbmi > nashbmi) {
      console.log ("John's BMI (" + johnbmi.toFixed(1) + ") is higher than Nash's (" + nashbmi.toFixed(1) +")");
    } else {
      console.log ("Nash's BMI (" + nashbmi.toFixed(1) + ") is higher than John's (" + johnbmi.toFixed(1) + ")");
    }


