//  #7 Looping

let data = [10, 20, 30, 40, 50];
let total = 0;

/* You code here (you are allowed to reassigned the variable) 
Maybe you can write 3 lines or more
Use for, do while, while for, or forEach
*/

for (let i = 0; i < data.length; i++){
    total += data[i];
}


console.log(`Jumlah total = ${total}`);

/* 
Jumlah total = 150
*/
