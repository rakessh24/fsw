/* #5 Buat function untuk menghitung BMI (Body Mass Index)
- BMI = mass / height ** 2 = mass / (height * height) (mass in kg and height in meter)
- Steven weights 78 kg and is 1.69 m tall. Bill weights 92 kg and is 1.95 
m tall
*/

function BMI(mass, height){
    let bmi = mass / (height ** 2);
    return bmi;
}

// Menghitung BMI Steven
let stevenmass = 78;
let stevenheight = 1.69;
let stevenbmi = BMI(stevenmass, stevenheight);
console.log("Steven BMI: ", stevenbmi);

// Menghitung BMI Bill
let billmass = 92;
let billheight = 1.95;
let billbmi = BMI(billmass, billheight);
console.log("Bill BMI: ", billbmi);