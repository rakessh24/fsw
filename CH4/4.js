// #4 Buat agar awal kalimat jadi kapital
const message = "Sampaikan pada Sabrina belajar Javascript sangat menyenangkan";

let text1 = message.slice(23,24).toUpperCase();
let text2 = message.slice(24);

console.log(text1 + text2);

/* 
Output: Belajar Javascript sangat menyenangkan
*/